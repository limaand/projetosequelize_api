module.exports = (sequelize, DataTypes) => {
    const ContentyCategory = sequelize.define('contenty_category', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        title: DataTypes.STRING,
        style: DataTypes.STRING,
        icon: DataTypes.STRING
    }, {
        freezeTableName: true,
    });

    ContentyCategory.associate = (models) => {
        ContentyCategory.hasMany(models.contenty, { onDelete: 'cascade' });
    };

    return ContentyCategory;
}