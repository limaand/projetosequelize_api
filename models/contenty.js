module.exports = (sequelize, DataTypes) => {
    const Contenty = sequelize.define('contenty', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        title: DataTypes.STRING,
        content: {
            type: DataTypes.TEXT,
            allowNull: false
        },
        imagem: DataTypes.STRING,

    }, {
        freezeTableName: true,
    });

    Contenty.associate = (models) => {
        Contenty.belongsTo(models.contenty_category);
    };

    return Contenty;
}