module.exports = (sequelize, DataTypes) => {
    const Setor = sequelize.define('setor', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        nome: {
            type: DataTypes.STRING,
            allowNull: false
        }
    }, {
        freezeTableName: true,
    });

    Setor.associate = (models) => {
        Setor.hasMany(models.contato);
    };

    return Setor;
}