module.exports = (sequelize, DataTypes) => {
    const Arquivo = sequelize.define('arquivo', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },

        titulo: {
            type: DataTypes.STRING,
            allowNull: false
        },
        descricao: DataTypes.STRING,
        slug: DataTypes.STRING,
        arquivo: {
            type: DataTypes.TEXT,
            allowNull: false
        },


    }, {
        freezeTableName: true,
    });

    Arquivo.associate = (models) => {
        Arquivo.belongsTo(models.area);
    };

    return Arquivo;
}