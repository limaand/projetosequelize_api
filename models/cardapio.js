module.exports = (sequelize, DataTypes) => {
    const Cardapio = sequelize.define('cardapio', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        title: DataTypes.STRING,
        imagem: DataTypes.STRING
    }, {
        freezeTableName: true,
    });


    return Cardapio;
}