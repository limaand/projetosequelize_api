module.exports = (sequelize, DataTypes) => {
    const Contato = sequelize.define('contato', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },

        nome: DataTypes.STRING,
        telefone: DataTypes.STRING,
        email: DataTypes.STRING
    }, {
        freezeTableName: true,
    });

    Contato.associate = (models) => {
        Contato.belongsTo(models.setor);
    };

    return Contato;
}