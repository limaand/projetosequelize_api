module.exports = (sequelize, DataTypes) => {
    const Area = sequelize.define('area', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        nome: DataTypes.STRING,
        slug: DataTypes.STRING
    }, {
        freezeTableName: true,
    });

    Area.associate = (models) => {
        Area.hasMany(models.arquivo, { onDelete: 'cascade' });
    };

    return Area;
}