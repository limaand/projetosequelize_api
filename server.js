const express = require("express");
const bodyParser = require("body-parser");
const exphbs = require('express-handlebars')
const faker = require("faker");
const times = require("lodash.times");
const random = require("lodash.random");
const path = require('path')
const db = require("./models");

const apiPost = require("./app/api/post");
const apiAuthor = require("./app/api/author");

const app = express();

app.engine('handlebars', exphbs({ defaultLayout: 'main' }))
app.set('view engine', 'handlebars')

//set static folder
app.use(express.static(path.join(__dirname, 'app/public')))
    //app.use(express.static("app/public"));

//bodyParser
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json());


apiPost(app, db);
apiAuthor(app, db);





db.sequelize.sync().then(() => {
    const PORT = process.env.PORT || 5000
    app.listen(PORT, console.log(`Server started on port ${PORT}`))

});